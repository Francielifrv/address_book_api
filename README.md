## Address book API 

#### Dependencies
* Java 11
* Postgres
* [Gradle](https://gradle.org/)

#### Running tests

`gradle test` or
`./gradlew test` (if you don't have gradle installed you may run gradlew script instead. It may take some time when running for the first time.)

#### Building the application

`gradle build` or
`./gradlew build`

#### Running the application

`sh create_database.sh` - creates database. Make sure postgres is up when executing this step.

`gradle bootRun` or `./gradlew bootRun` - starts the application

#### Documentation

API documentation can be found on http://localhost:8080/swagger-ui.html#/customer-controller. 
