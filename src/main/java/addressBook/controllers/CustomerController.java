package addressBook.controllers;

import addressBook.models.customer.CustomerRequest;
import addressBook.models.customer.CustomerResponse;
import addressBook.models.customer.CustomerService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    private CustomerService service;

    @Autowired
    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @PostMapping
    @ApiResponses(value = {
            @ApiResponse( code = 201, message = "Created"),
            @ApiResponse(code= 400, message = "Bad request")
    })
    public ResponseEntity<CustomerResponse> createCustomer(@Valid @RequestBody CustomerRequest request) {
        return service.createCustomer(request);
    }

    @GetMapping("/{surname}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Not Found")
    })
    public ResponseEntity<List<CustomerResponse>> fetchByCustomerSurname(@PathVariable("surname") String surname) {
        return service.fetchContactBySurname(surname);
    }
}
