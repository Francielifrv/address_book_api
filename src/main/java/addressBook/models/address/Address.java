package addressBook.models.address;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Address {
    public Address() {

    }

    public Address(String street, Integer houseNumber, String postCode, String city, String country) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.postCode = postCode;
        this.city = city;
        this.country = country;
    }

    public Address(Long id, String street, Integer houseNumber, String postCode, String city, String country) {
        this(street, houseNumber, postCode, city, country);
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_seq_gen")
    private Long id;
    private String street;
    private Integer houseNumber;
    private String postCode;
    private String city;
    private String country;

    public static Address fromRequest(AddressRequest request) {
        if (request == null) {
            return new Address();
        }
        return new Address(request.getStreet(), request.getHouseNumber(), request.getPostCode(), request.getCity(),
                request.getCountry());
    }

    public Long getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}
