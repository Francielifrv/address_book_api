package addressBook.models.address;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddressRequest {
    private final String street;
    private final Integer houseNumber;
    private final String postCode;
    private final String city;
    private final String country;

    public AddressRequest(@JsonProperty("street") String street, @JsonProperty("house_number") Integer houseNumber,
                          @JsonProperty("post_code") String postCode, @JsonProperty("city") String city,
                          @JsonProperty("country") String country) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.postCode = postCode;
        this.city = city;
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}
