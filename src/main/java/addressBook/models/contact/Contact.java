package addressBook.models.contact;

import addressBook.models.address.Address;

import javax.persistence.*;

@Entity
public class Contact {
    public Contact() {
    }

    public Contact(String email, String phoneNumber, String mobileNumber, Address address) {
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.mobileNumber = mobileNumber;
        this.address = address;
    }

    public Contact(Long id, String email, String phoneNumber, String mobileNumber, Address address) {
        this(email, phoneNumber, mobileNumber, address);
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_seq_gen")
    private Long id;
    private String email;
    private String phoneNumber;
    private String mobileNumber;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true)
    private Address address;

    public static Contact fromRequest(ContactRequest request) {
        if (request == null) {
            return new Contact();
        }

        Address address = request.getAddress() != null ? Address.fromRequest(request.getAddress()) : null;

        return new Contact(request.getEmail(), request.getPhoneNumber(), request.getMobileNumber(), address);
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public Address getAddress() {
        return address;
    }
}
