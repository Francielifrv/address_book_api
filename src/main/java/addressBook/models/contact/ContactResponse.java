package addressBook.models.contact;

import addressBook.models.address.Address;
import addressBook.models.address.AddressResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ContactResponse {
    private final Long id;
    private final String email;
    private final String phoneNumber;
    private final String mobileNumber;
    private final AddressResponse address;

    public ContactResponse(@JsonProperty("id") Long id, @JsonProperty("email") String email,
                           @JsonProperty("phone_number") String phoneNumber,
                           @JsonProperty("mobile_number") String mobileNumber,
                           @JsonProperty("address") AddressResponse address) {
        this.id = id;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.mobileNumber = mobileNumber;
        this.address = address;
    }

    public static ContactResponse fromEntity(Contact entity) {
        Address address = entity.getAddress();
        AddressResponse addressResponse = address == null ? null : AddressResponse.fromEntity(entity.getAddress());

        return new ContactResponse(entity.getId(), entity.getEmail(), entity.getPhoneNumber(), entity.getMobileNumber(),
                addressResponse);
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public AddressResponse getAddress() {
        return address;
    }
}
