package addressBook.models.contact;

import addressBook.models.address.AddressRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;

public class ContactRequest {
    @Email(message = "Email must match format email@example.co")
    private final String email;
    private final String phoneNumber;
    private final String mobileNumber;
    private final AddressRequest address;

    public ContactRequest(@JsonProperty("email") String email, @JsonProperty("phone_number") String phoneNumber,
                          @JsonProperty("mobile_number") String mobileNumber,
                          @JsonProperty("address") AddressRequest address) {
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.mobileNumber = mobileNumber;
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public AddressRequest getAddress() {
        return address;
    }
}
