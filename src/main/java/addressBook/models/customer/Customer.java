package addressBook.models.customer;

import addressBook.models.contact.Contact;
import addressBook.models.contact.ContactRequest;

import javax.persistence.*;

@Entity
public class Customer {

    public Customer() {
    }

    public Customer(String givenName, String surname, Contact contact) {
        this.givenName = givenName;
        this.surname = surname;
        this.contact = contact;
    }

    public Customer(Long id, String givenName, String surname, Contact contact) {
        this(givenName, surname, contact);
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_seq_gen")
    private Long id;

    private String givenName;
    private String surname;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true)
    private Contact contact;

    public static Customer fromRequest(CustomerRequest request) {
        if (request == null) {
            return new Customer();
        }

        ContactRequest contactRequest = request.getContact();
        Contact contact = contactRequest == null ? null : Contact.fromRequest(contactRequest);

        return new Customer(request.getGivenName(), request.getSurname(), contact);
    }

    public Long getId() {
        return id;
    }

    public String getGivenName() {
        return givenName;
    }

    public String getSurname() {
        return surname;
    }

    public Contact getContact() {
        return contact;
    }
}
