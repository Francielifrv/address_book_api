package addressBook.models.customer;

import addressBook.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {
    private CustomerRepository repository;

    @Autowired
    public CustomerService(CustomerRepository repository) {
        this.repository = repository;
    }

    public ResponseEntity<CustomerResponse> createCustomer(CustomerRequest request) {
        Customer customer = Customer.fromRequest(request);

        Customer persistedCustomer = repository.save(customer);

        CustomerResponse response = CustomerResponse.fromEntity(persistedCustomer);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    public ResponseEntity<List<CustomerResponse>> fetchContactBySurname(String surname) {
        List<Customer> customers = repository.findBySurname(surname);

        if (customers != null && !customers.isEmpty()) {
            List<CustomerResponse> contactResponses = customers.stream()
                    .map(CustomerResponse::fromEntity)
                    .collect(Collectors.toList());

            return new ResponseEntity<>(contactResponses, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
