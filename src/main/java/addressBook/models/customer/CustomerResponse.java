package addressBook.models.customer;

import addressBook.models.contact.Contact;
import addressBook.models.contact.ContactResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerResponse {
    private Long id;
    private final String givenName;
    private final String surname;
    private final ContactResponse contact;

    public CustomerResponse(@JsonProperty("id") Long id, @JsonProperty("given_name") String givenName,
                            @JsonProperty("surname") String surname, @JsonProperty("contact") ContactResponse contact) {
        this.id = id;
        this.givenName = givenName;
        this.surname = surname;
        this.contact = contact;
    }

    public static CustomerResponse fromEntity(Customer entity) {
        Contact contact = entity.getContact();
        ContactResponse contactResponse = contact == null ? null : ContactResponse.fromEntity(entity.getContact());

        return new CustomerResponse(entity.getId(), entity.getGivenName(), entity.getSurname(), contactResponse);
    }

    public Long getId() {
        return id;
    }

    public String getGivenName() {
        return givenName;
    }

    public String getSurname() {
        return surname;
    }

    public ContactResponse getContact() {
        return contact;
    }
}
