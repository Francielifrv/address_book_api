package addressBook.models.customer;

import addressBook.models.contact.ContactRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

public class CustomerRequest {
    @NotBlank(message = "Given name must be filled")
    private final String givenName;

    @NotBlank(message = "Surname must be filled")
    private final String surname;

    @Valid
    private final ContactRequest contact;

    public CustomerRequest(@JsonProperty("given_name") String givenName, @JsonProperty("surname") String surname,
                           @JsonProperty("contact") ContactRequest contact) {
        this.givenName = givenName;
        this.surname = surname;
        this.contact = contact;
    }

    public String getGivenName() {
        return givenName;
    }

    public String getSurname() {
        return surname;
    }

    public ContactRequest getContact() {
        return contact;
    }
}
