package addressBook.models;

import addressBook.models.address.Address;
import addressBook.models.address.AddressRequest;
import addressBook.models.contact.Contact;
import addressBook.models.contact.ContactRequest;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class ContactTest {
    private final String EMAIL = "a@email.com";
    private final String PHONE_NUMBER = "39349302";
    private final String MOBILE_NUMBER = "9302394";
    private final AddressRequest ADDRESS = new AddressRequest("street_name", 23, "postal_code", "city",
            "country");

    @Test
    public void buildsContactFromRequestObject() {
        ContactRequest request = new ContactRequest(EMAIL, PHONE_NUMBER, MOBILE_NUMBER, ADDRESS);

        Contact contact = Contact.fromRequest(request);

        assertThat(contact.getId(), is(nullValue()));
        assertThat(contact.getEmail(), is(EMAIL));
        assertThat(contact.getMobileNumber(), is(MOBILE_NUMBER));
        assertThat(contact.getPhoneNumber(), is(PHONE_NUMBER));
        assertThat(contact.getAddress(), is(notNullValue()));
    }

    @Test
    public void buildsWithCorrectAddress() {
        ContactRequest request = new ContactRequest(EMAIL, PHONE_NUMBER, MOBILE_NUMBER, ADDRESS);

        Contact contact = Contact.fromRequest(request);

        Address contactAddress = contact.getAddress();

        assertThat(contactAddress.getId(), is(nullValue()));
        assertThat(contactAddress.getStreet(), is(ADDRESS.getStreet()));
        assertThat(contactAddress.getHouseNumber(), is(ADDRESS.getHouseNumber()));
        assertThat(contactAddress.getPostCode(), is(ADDRESS.getPostCode()));
        assertThat(contactAddress.getCity(), is(ADDRESS.getCity()));
        assertThat(contactAddress.getCountry(), is(ADDRESS.getCountry()));
    }

    @Test
    public void returnsEmptyObjectWhenRequestIsNull() {
        ContactRequest nullRequest = null;

        Contact contact = Contact.fromRequest(nullRequest);

        assertThat(contact, is(notNullValue()));
        assertThat(contact.getAddress(), is(nullValue()));
        assertThat(contact.getPhoneNumber(), is(nullValue()));
        assertThat(contact.getMobileNumber(), is(nullValue()));
        assertThat(contact.getEmail(), is(nullValue()));
        assertThat(contact.getId(), is(nullValue()));
    }
}