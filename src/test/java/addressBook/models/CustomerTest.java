package addressBook.models;

import addressBook.models.contact.Contact;
import addressBook.models.contact.ContactRequest;
import addressBook.models.customer.Customer;
import addressBook.models.customer.CustomerRequest;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

public class CustomerTest {
    private static final String GIVEN_NAME = "John";
    private static final String SURNAME = "Doe";
    private static final ContactRequest CONTACT_REQUEST = buildContactRequest();
    private static final CustomerRequest REQUEST = new CustomerRequest(GIVEN_NAME, SURNAME, CONTACT_REQUEST);


    @Test
    public void buildsCustomerFromRequestObject() {
        Customer customer = Customer.fromRequest(REQUEST);

        assertThat(customer.getId(), is(nullValue()));
        assertThat(customer.getGivenName(), is(GIVEN_NAME));
        assertThat(customer.getSurname(), is(SURNAME));
        assertThat(customer.getContact(), is(notNullValue()));
    }

    @Test
    public void buildsContactUsingRequestData() {
        Customer customer = Customer.fromRequest(REQUEST);
        Contact customerContact = customer.getContact();

        assertThat(customerContact.getId(), is(nullValue()));
        assertThat(customerContact.getEmail(), is(CONTACT_REQUEST.getEmail()));
        assertThat(customerContact.getMobileNumber(), is(CONTACT_REQUEST.getMobileNumber()));
        assertThat(customerContact.getPhoneNumber(), is(CONTACT_REQUEST.getPhoneNumber()));
        assertThat(customerContact.getEmail(), is(CONTACT_REQUEST.getEmail()));
        assertThat(customerContact.getAddress(), is(nullValue()));
    }

    @Test
    public void returnsEmptyObjectWhenRequestIsNull() {
        Customer customer = Customer.fromRequest(null);

        assertThat(customer, is(notNullValue()));
        assertThat(customer.getSurname(), is(nullValue()));
        assertThat(customer.getGivenName(), is(nullValue()));
        assertThat(customer.getContact(), is(nullValue()));
        assertThat(customer.getId(), is(nullValue()));

    }

    private static ContactRequest buildContactRequest() {
        return new ContactRequest("email", "934803", "0293493", null);
    }
}