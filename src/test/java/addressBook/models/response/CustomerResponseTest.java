package addressBook.models.response;

import addressBook.models.address.Address;
import addressBook.models.contact.Contact;
import addressBook.models.customer.Customer;
import addressBook.models.contact.ContactResponse;
import addressBook.models.customer.CustomerResponse;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class CustomerResponseTest {
    private static final Long CUSTOMER_ID = 897L;
    private static final String GIVEN_NAME = "John";
    private static final String SURNAME = "Doe";

    private final Long CONTACT_ID = 9L;
    private final String EMAIL = "email@example.com";
    private final String PHONE_NUMBER = "3940289";
    private final String MOBILE_NUMBER = "38942";

    @Test
    public void buildsResponseFromEntity() {
        Customer entity = buildCustomer();

        CustomerResponse response = CustomerResponse.fromEntity(entity);

        assertThat(response.getId(), is(CUSTOMER_ID));
        assertThat(response.getGivenName(), is(GIVEN_NAME));
        assertThat(response.getSurname(), is(SURNAME));
        assertThat(response.getContact(), is(notNullValue()));
    }

    @Test
    public void buildsContactWithCorrectInformation() {
        Customer entity = buildCustomer();

        CustomerResponse response = CustomerResponse.fromEntity(entity);

        ContactResponse contactResponse = response.getContact();

        assertThat(contactResponse.getId(), is(CONTACT_ID));
        assertThat(contactResponse.getPhoneNumber(), is(PHONE_NUMBER));
        assertThat(contactResponse.getMobileNumber(), is(MOBILE_NUMBER));
        assertThat(contactResponse.getEmail(), is(EMAIL));
    }

    private Customer buildCustomer() {
        Address address = new Address(12L, "street", 89, "postal_code", "city", "country");
        Contact contact = new Contact(CONTACT_ID, EMAIL, PHONE_NUMBER, MOBILE_NUMBER, address);
        return new Customer(CUSTOMER_ID, GIVEN_NAME, SURNAME, contact);
    }
}