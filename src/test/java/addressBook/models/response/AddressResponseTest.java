package addressBook.models.response;

import addressBook.models.address.Address;
import addressBook.models.address.AddressResponse;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class AddressResponseTest {

    private static final long ADDRESS_ID = 1L;
    private static final String STREET = "street";
    private static final int HOUSE_NUMBER = 102;
    private static final String POSTAL_CODE = "postal code";
    private static final String COUNTRY = "country";
    private static final String CITY = "city";

    @Test
    public void createsResponseFromEntity() {
        Address entity = new Address(ADDRESS_ID, STREET, HOUSE_NUMBER, POSTAL_CODE, CITY, COUNTRY);

        AddressResponse response = AddressResponse.fromEntity(entity);

        assertThat(response.getId(), is(ADDRESS_ID));
        assertThat(response.getStreet(), is(STREET));
        assertThat(response.getHouseNumber(), is(HOUSE_NUMBER));
        assertThat(response.getCity(), is(CITY));
        assertThat(response.getCountry(), is(COUNTRY));
        assertThat(response.getPostCode(), is(POSTAL_CODE));
    }
}