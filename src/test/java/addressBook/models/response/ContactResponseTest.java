package addressBook.models.response;

import addressBook.models.address.Address;
import addressBook.models.contact.Contact;
import addressBook.models.address.AddressResponse;
import addressBook.models.contact.ContactResponse;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class ContactResponseTest {
    private final Long CONTACT_ID = 9L;
    private final String EMAIL = "email@example.com";
    private final String PHONE_NUMBER = "3940289";
    private final String MOBILE_NUMBER = "38942";

    private static final long ADDRESS_ID = 1L;
    private static final String STREET = "street";
    private static final int HOUSE_NUMBER = 102;
    private static final String POSTAL_CODE = "postal code";
    private static final String COUNTRY = "country";
    private static final String CITY = "city";

    private static Address address;

    @Before
    public void setUp() {
        address = new Address(ADDRESS_ID, STREET, HOUSE_NUMBER, POSTAL_CODE, CITY, COUNTRY);
    }

    @Test
    public void buildResponseFromEntityObject() {
        Contact entity = new Contact(CONTACT_ID, EMAIL, PHONE_NUMBER, MOBILE_NUMBER, address);

        ContactResponse response = ContactResponse.fromEntity(entity);

        assertThat(response.getId(), is(CONTACT_ID));
        assertThat(response.getEmail(), is(EMAIL));
        assertThat(response.getMobileNumber(), is(MOBILE_NUMBER));
        assertThat(response.getPhoneNumber(), is(PHONE_NUMBER));
        assertThat(response.getAddress(), is(notNullValue()));
    }

    @Test
    public void buildsAddressCorrectly() {
        Contact entity = new Contact(CONTACT_ID, EMAIL, PHONE_NUMBER, MOBILE_NUMBER, address);

        ContactResponse response = ContactResponse.fromEntity(entity);

        AddressResponse addressResponse = response.getAddress();

        assertThat(addressResponse, is(notNullValue()));
        assertThat(addressResponse.getId(), is(ADDRESS_ID));
        assertThat(addressResponse.getStreet(), is(STREET));
        assertThat(addressResponse.getHouseNumber(), is(HOUSE_NUMBER));
        assertThat(addressResponse.getPostCode(), is(POSTAL_CODE));
        assertThat(addressResponse.getCity(), is(CITY));
        assertThat(addressResponse.getCountry(), is(COUNTRY));
    }

    @Test
    public void setsAddressToNullIfNotPresentOnEntity() {
        Contact entity = new Contact(CONTACT_ID, EMAIL, PHONE_NUMBER, MOBILE_NUMBER, null);

        ContactResponse response = ContactResponse.fromEntity(entity);

        AddressResponse addressResponse = response.getAddress();

        assertThat(addressResponse, is(nullValue()));
    }
}