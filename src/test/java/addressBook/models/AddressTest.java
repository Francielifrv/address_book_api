package addressBook.models;

import addressBook.models.address.Address;
import addressBook.models.address.AddressRequest;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class AddressTest {

    private static final String STREET_NAME = "street_name";
    private static final int HOUSE_NUMBER = 23;
    private static final String POSTAL_CODE = "postal_code";
    private static final String CITY = "city";
    private static final String COUNTRY = "country";

    @Test
    public void buildsAddressFromRequest() {
        AddressRequest request = new AddressRequest(STREET_NAME, HOUSE_NUMBER, POSTAL_CODE, CITY, COUNTRY);

        Address address = Address.fromRequest(request);

        assertThat(address.getId(), is(nullValue()));
        assertThat(address.getStreet(), is(STREET_NAME));
        assertThat(address.getHouseNumber(), is(HOUSE_NUMBER));
        assertThat(address.getPostCode(), is(POSTAL_CODE));
        assertThat(address.getCity(), is(CITY));
        assertThat(address.getCountry(), is(COUNTRY));
    }

    @Test
    public void returnsEmptyAddressWhenRequestIsNull() {
        AddressRequest nullRequest = null;

        Address address = Address.fromRequest(nullRequest);

        assertThat(address, is(notNullValue()));
        assertThat(address.getId(), is(nullValue()));
        assertThat(address.getStreet(), is(nullValue()));
        assertThat(address.getHouseNumber(), is(nullValue()));
        assertThat(address.getPostCode(), is(nullValue()));
        assertThat(address.getCountry(), is(nullValue()));
        assertThat(address.getCity(), is(nullValue()));
    }
}