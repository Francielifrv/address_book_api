package addressBook.services;

import addressBook.models.address.Address;
import addressBook.models.contact.Contact;
import addressBook.models.customer.Customer;
import addressBook.models.address.AddressRequest;
import addressBook.models.contact.ContactRequest;
import addressBook.models.customer.CustomerRequest;
import addressBook.models.address.AddressResponse;
import addressBook.models.contact.ContactResponse;
import addressBook.models.customer.CustomerResponse;
import addressBook.models.customer.CustomerService;
import addressBook.repositories.CustomerRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class CustomerServiceTest {
    private static final Long ADDRESS_ID = 23L;
    private static final String STREET = "street";
    private static final int HOUSE_NUMBER = 2;
    private static final String POSTAL_CODE = "postal_code";
    private static final String CITY = "city";
    private static final String COUNTRY = "country";

    private static final Long CONTACT_ID = 23L;
    private static final String EMAIL = "email@email.com";
    private static final String PHONE_NUMBER = "23434";
    private static final String MOBILE_NUMBER = "9392";

    private static final Long CUSTOMER_ID = 93L;
    private static final String GIVEN_NAME = "John";
    private static final String SURNAME = "Doe";


    @Mock
    private CustomerRepository repository;

    private Customer customerToPersist;
    private CustomerRequest request;

    private CustomerService service;

    @Before
    public void setUp() {
        initMocks(this);

        service = new CustomerService(repository);
        request = createCustomerRequest();

        customerToPersist = createCustomer(null, null, null);
        when(repository.save(any(Customer.class))).thenReturn(customerToPersist);

        Customer foundCustomer = createCustomer(CUSTOMER_ID, ADDRESS_ID, CONTACT_ID);
        when(repository.findBySurname(anyString())).thenReturn(Collections.singletonList(foundCustomer));
    }

    @Test
    public void persistsCustomer() {
        service.createCustomer(request);

        verify(repository, times(1)).save(any(Customer.class));
    }

    @Test
    public void persistsCorrectCustomerInformation() {
        ArgumentCaptor<Customer> customerArgumentCaptor = ArgumentCaptor.forClass(Customer.class);

        service.createCustomer(request);

        verify(repository, times(1)).save(customerArgumentCaptor.capture());

        Customer persistedCustomer = customerArgumentCaptor.getValue();

        assertThat(persistedCustomer.getGivenName(), is(GIVEN_NAME));
        assertThat(persistedCustomer.getSurname(), is(SURNAME));

        Contact persistedContact = persistedCustomer.getContact();
        assertThat(persistedContact.getEmail(), is(EMAIL));
        assertThat(persistedContact.getPhoneNumber(), is(PHONE_NUMBER));
        assertThat(persistedContact.getMobileNumber(), is(MOBILE_NUMBER));

        Address persistedAddress = persistedContact.getAddress();
        assertThat(persistedAddress, samePropertyValuesAs(customerToPersist.getContact().getAddress()));
    }

    @Test
    public void returnsPersistedCustomerResponse() {
        CustomerResponse response = service.createCustomer(request).getBody();

        assertThat(response.getGivenName(), is(GIVEN_NAME));
        assertThat(response.getSurname(), is(SURNAME));

        ContactResponse contactResponse = response.getContact();
        assertThat(contactResponse.getEmail(), is(EMAIL));
        assertThat(contactResponse.getMobileNumber(), is(MOBILE_NUMBER));
        assertThat(contactResponse.getPhoneNumber(), is(PHONE_NUMBER));

        AddressResponse addressResponse = contactResponse.getAddress();
        assertThat(addressResponse.getStreet(), is(STREET));
        assertThat(addressResponse.getCity(), is(CITY));
        assertThat(addressResponse.getPostCode(), is(POSTAL_CODE));
        assertThat(addressResponse.getCountry(), is(COUNTRY));
        assertThat(addressResponse.getHouseNumber(), is(HOUSE_NUMBER));
    }

    @Test
    public void returnsHttpStatus201WhenCustomerIsCreated() {
        ResponseEntity<CustomerResponse> response = service.createCustomer(request);

        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
    }

    @Test
    public void fetchesContactInformationBysCustomerSurnameFromDatabase() {
        service.fetchContactBySurname(SURNAME);

        verify(repository).findBySurname(SURNAME);
    }

    @Test
    public void returnsContactInformation() {
        ResponseEntity<List<CustomerResponse>> response = service.fetchContactBySurname(SURNAME);

        CustomerResponse customerResponse = Objects.requireNonNull(response.getBody()).get(0);
        assertThat(customerResponse.getId(), is(CUSTOMER_ID));
        assertThat(customerResponse.getSurname(), is(SURNAME));
        assertThat(customerResponse.getGivenName(), is(GIVEN_NAME));

        ContactResponse contactResponse = customerResponse.getContact();
        assertThat(contactResponse.getId(), is(CONTACT_ID));
        assertThat(contactResponse.getPhoneNumber(), is(PHONE_NUMBER));
        assertThat(contactResponse.getMobileNumber(), is(MOBILE_NUMBER));
        assertThat(contactResponse.getEmail(), is(EMAIL));

        AddressResponse address = contactResponse.getAddress();
        assertThat(address.getId(), is(ADDRESS_ID));
        assertThat(address.getHouseNumber(), is(HOUSE_NUMBER));
        assertThat(address.getStreet(), is(STREET));
        assertThat(address.getPostCode(), is(POSTAL_CODE));
        assertThat(address.getCity(), is(CITY));
        assertThat(address.getCountry(), is(COUNTRY));
    }

    @Test
    public void returnsHttpStatus200WhenContactIsFoundBySurname() {
        ResponseEntity<List<CustomerResponse>> response = service.fetchContactBySurname(SURNAME);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void returnsHttpStatus404WhenThereIsNoContactForThatSurname() {
        when(repository.findBySurname(SURNAME)).thenReturn(null);

        ResponseEntity<List<CustomerResponse>> response = service.fetchContactBySurname(SURNAME);

        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }

    private Customer createCustomer(Long customerId, Long addressId, Long contactId) {
        Address address = new Address(addressId, STREET, HOUSE_NUMBER, POSTAL_CODE, CITY, COUNTRY);
        Contact contact = new Contact(contactId, EMAIL, PHONE_NUMBER, MOBILE_NUMBER, address);

        return new Customer(customerId, GIVEN_NAME, SURNAME, contact);
    }

    private CustomerRequest createCustomerRequest() {
        AddressRequest address = new AddressRequest(STREET, HOUSE_NUMBER, POSTAL_CODE, CITY, COUNTRY);
        ContactRequest contact = new ContactRequest(EMAIL, PHONE_NUMBER, MOBILE_NUMBER, address);

        return new CustomerRequest(GIVEN_NAME, SURNAME, contact);
    }
}