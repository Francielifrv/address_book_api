package addressBook.controllers;

import addressBook.models.address.AddressRequest;
import addressBook.models.contact.ContactRequest;
import addressBook.models.customer.CustomerRequest;
import addressBook.models.address.AddressResponse;
import addressBook.models.contact.ContactResponse;
import addressBook.models.customer.CustomerResponse;
import addressBook.models.customer.CustomerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class CustomerControllerTest {
    private static final long ADDRESS_ID = 123L;
    private static final String COUNTRY = "country";
    private static final String STREET_NAME = "street name";
    private static final int HOUSE_NUMBER = 302;
    private static final String POSTAL_CODE = "postal code";
    private static final String CITY = "city";

    private static final long CONTACT_ID = 83L;
    private static final String EMAIL = "email";
    private static final String PHONE_NUMBER = "phone number";
    private static final String MOBILE = "mobile";

    private static final String SURNAME = "surname";
    private static final String GIVEN_NAME = "givenName";
    private static final long CUSTOMER_ID = 9304L;

    @Mock
    CustomerService service;

    private CustomerRequest request;
    private ResponseEntity<CustomerResponse> createdCustomerResponse;
    private ResponseEntity<List<CustomerResponse>> fetchCustomerResponse;

    private CustomerController controller;

    @Before
    public void setUp() {
        initMocks(this);

        controller = new CustomerController(service);

        request = createCustomerRequest();

        CustomerResponse customerResponse = createCustomerResponse();
        createdCustomerResponse = new ResponseEntity<>(customerResponse, HttpStatus.CREATED);
        when(service.createCustomer(any(CustomerRequest.class))).thenReturn(createdCustomerResponse);

        fetchCustomerResponse = new ResponseEntity<>(Collections.singletonList(customerResponse), HttpStatus.OK);

        when(service.fetchContactBySurname(anyString())).thenReturn(fetchCustomerResponse);
    }

    @Test
    public void returnsHttpStatusCreatedWhenCustomerIsSuccessfullyCreated() {
        ResponseEntity<CustomerResponse> response = controller.createCustomer(request);

        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
    }

    @Test
    public void createsCustomer() {
        ResponseEntity<CustomerResponse> response = controller.createCustomer(request);

        verify(service, times(1)).createCustomer(request);
    }

    @Test
    public void returnsCreatedCustomerInformation() {
        ResponseEntity<CustomerResponse> createCustomerResponse = controller.createCustomer(request);

        assertThat(createCustomerResponse, is(createdCustomerResponse));
    }

    @Test
    public void fetchesContactBySurname() {
        controller.fetchByCustomerSurname(SURNAME);

        verify(service).fetchContactBySurname(SURNAME);
    }

    @Test
    public void returnsContactInformationBySurname() {
        ResponseEntity<List<CustomerResponse>> serviceResponse = controller.fetchByCustomerSurname(SURNAME);

        assertThat(serviceResponse, is(fetchCustomerResponse));
    }


    private CustomerRequest createCustomerRequest() {
        AddressRequest address = new AddressRequest(STREET_NAME, HOUSE_NUMBER, POSTAL_CODE, CITY, COUNTRY);
        ContactRequest contact = new ContactRequest(EMAIL, PHONE_NUMBER, MOBILE, address);

        return new CustomerRequest(GIVEN_NAME, SURNAME, contact);
    }

    private CustomerResponse createCustomerResponse() {
        AddressResponse address = new AddressResponse(ADDRESS_ID, STREET_NAME, HOUSE_NUMBER, POSTAL_CODE, CITY, COUNTRY);
        ContactResponse contact = new ContactResponse(CONTACT_ID, EMAIL, PHONE_NUMBER, MOBILE, address);

        return new CustomerResponse(CUSTOMER_ID, GIVEN_NAME, SURNAME, contact);
    }
}